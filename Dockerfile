FROM nginx:latest
ARG git_commit
COPY ./public /usr/share/nginx/html
RUN echo $git_commit > /git_commit.txt
