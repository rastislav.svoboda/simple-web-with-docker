using System;

namespace Catalog.Entities
{
    public class Manifest
    {
        public Guid Manifest_Id { get; set; }
        public string Alias { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public string Documentation { get; set; }
        public string Name { get; set; }
        public string Project { get; set; }
        public DateTime Published { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public string Version { get; set; }
        public string Json_Data { get; set; }
        public Guid Transakcia_Id { get; set; }
        public Guid Transakcia_Zrusene_Id { get; set; }
        public DateTime Changed { get; set; }
    }
}