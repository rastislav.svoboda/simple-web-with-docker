using Catalog.Dtos;
using Catalog.Entities;

namespace Catalog
{
    public static class Extensions
    {
        public static ItemDto AsDto(this Item item)
        {
            return new ItemDto(item.Id, item.Name, item.Description, item.Price);
        }
        public static ManifestDto AsDto(this Manifest item)
        {
            return new ManifestDto(item.Manifest_Id, item.Alias, item.Author, item.Description,
                item.Documentation, item.Name, item.Project, item.Published, item.Type, item.Url, item.Version,
                item.Json_Data, item.Transakcia_Id, item.Transakcia_Zrusene_Id, item.Changed
            );

        }
    }
}