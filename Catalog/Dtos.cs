using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json;

namespace Catalog.Dtos
{
    public record ItemDto(int Id, string Name, string Description, int Price);

    public record CreateItemDto([Required] string Name, string Description, [Range(0, 1000)] int Price);

    public record UpdateItemDto([Required] string Name, string Description, [Range(0, 1000)] int Price);

    public record ManifestDto(Guid Manifest_Id, string Alias, string Author, string Description,
        string Documentation, string Name, string Project, DateTime Published, string Type, string Url, string Version,
        string JsonData, Guid TransakciaId, Guid TransakciaZruseneId, DateTime Changed);
    public record CreateManifestDto(string p_alias, string p_author, string p_description,
        string p_documentation, string p_name, string p_project, DateTime p_published, string p_type, string p_url, string p_version,
        string p_json_data, 
        Guid p_transakcia_id);}