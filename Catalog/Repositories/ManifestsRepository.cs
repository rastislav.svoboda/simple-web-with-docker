using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalog.Dtos;
using Catalog.Entities;
using Dapper;
using Npgsql;
using NpgsqlTypes;

namespace Catalog.Repositories
{
    public class ManifestsRepository
    {
        private NpgsqlConnection CreateConnection()
        {
            var connString = Environment.GetEnvironmentVariable("DB_CONNECTIONSTRING");

            //hack for debugging with only DB in docker
            if (connString == null)
                connString = "User ID=root;Password=password;Host=192.168.68.100;Port=5432;Database=docker;";
                    
                    
            // NpgsqlConnection.GlobalTypeMapper.u

            return new NpgsqlConnection(connString);
        }

        public async Task<IReadOnlyCollection<Manifest>> GetAllAsync()
        {
            try
            {
                using (var connection = CreateConnection())
                {
                    var allItems = await connection.QueryAsync<Manifest>("SELECT * FROM manifest;");
                    return allItems.ToList();
                }
            }
            catch (Exception)
            {
                //_logger.LogCritical(ex, "dapper failed");
                return null;
            }
        }

        // public async Task<Item> GetAsync(int id)
        // {
        //     try
        //     {
        //         using (var connection = CreateConnection())
        //         {
        //             var querySQL = string.Format(@"
        //                 SELECT * FROM items
        //                 WHERE id = {0};", id);
        //             var item = await connection.QueryFirstOrDefaultAsync<Item>(querySQL);
        //             return item;
        //         }
        //     }
        //     catch (Exception)
        //     {
        //         //_logger.LogCritical(ex, "dapper failed");
        //         return null;
        //     }
        // }

        public object Create(CreateManifestDto manifest)
        {
            using (var connection = CreateConnection())
            {
                connection.Open();
                var procedure = "manifest_ins3";



                // System.Data.DbType.stri
                // var js = NpgsqlDbType.Json;

                var p = new NpgsqlParameter();

                NpgsqlParameter[] parameters =
                    {
                  new NpgsqlParameter<string>("p_alias", manifest.p_alias),
                  new NpgsqlParameter<string>("p_author", manifest.p_alias),
                  new NpgsqlParameter<string>("p_description", manifest.p_alias),
                  new NpgsqlParameter<string>("p_documentation", manifest.p_alias),
                  new NpgsqlParameter<string>("p_name", manifest.p_alias),
                  new NpgsqlParameter<string>("p_project", manifest.p_alias),
                  new NpgsqlParameter<DateTime>("p_published", manifest.p_published),
                  new NpgsqlParameter<string>("p_type", manifest.p_alias),
                  new NpgsqlParameter<string>("p_alias", manifest.p_alias),
                  new NpgsqlParameter<string>("p_version", manifest.p_alias),
                  new NpgsqlParameter("p_json_data", NpgsqlDbType.Json){ Value = String.IsNullOrEmpty(manifest.p_json_data) ? DBNull.Value : (object)manifest.p_json_data },
                  new NpgsqlParameter("p_instance_id", DbType.Guid, 36){ Value = manifest.p_transakcia_id == null ? DBNull.Value : (object)manifest.p_transakcia_id },
                  };


                // p.Add("@p_alias", manifest.p_alias, dbType: System.Data.DbType.String, direction: ParameterDirection.Input);
                // p.Add("@p_author", manifest.p_author, dbType: System.Data.DbType.String, direction: ParameterDirection.Input);
                // p.Add("@p_description", manifest.p_description, dbType: System.Data.DbType.String, direction: ParameterDirection.Input);
                // p.Add("@p_documentation", manifest.p_documentation, dbType: System.Data.DbType.String, direction: ParameterDirection.Input);
                // p.Add("@p_name", manifest.p_name, dbType: System.Data.DbType.String, direction: ParameterDirection.Input);
                // p.Add("@p_project", manifest.p_project, dbType: System.Data.DbType.String, direction: ParameterDirection.Input);
                // p.Add("@p_published", manifest.p_published, direction: ParameterDirection.Input);
                // p.Add("@p_type", manifest.p_type, dbType: System.Data.DbType.String, direction: ParameterDirection.Input);
                // p.Add("@p_url", manifest.p_url, dbType: System.Data.DbType.String, direction: ParameterDirection.Input);
                // p.Add("@p_version", manifest.p_version, dbType: System.Data.DbType.String, direction: ParameterDirection.Input);
                // p.Add("@p_json_data", manifest.p_json_data, dbType: js, direction: ParameterDirection.Input);
                // p.Add("@p_transakcia_id", manifest.p_transakcia_id, dbType: System.Data.DbType.Guid, direction: ParameterDirection.Input);

                // var p = new 
                // {
                //     p_alias = manifest.p_alias,
                //     p_author = manifest.p_author
                // };
                // var p=manifest;

                var res = connection.Query(procedure, p, commandType: System.Data.CommandType.StoredProcedure).SingleOrDefault();

                return res;
            }
        }

        // public async Task<int> UpdateAsync(Item item)
        // {
        //     using (var connection = CreateConnection())
        //     {
        //         connection.Open();

        //         var updateSQL = @"
        //             UPDATE items 
        //             SET name = @name, description = @description, price = @price
        //             WHERE id = @id;";
        //         var res = await connection.ExecuteAsync(updateSQL, item);
        //         return res;
        //     }
        // }

        // public async Task<int> RemoveAsync(int id)
        // {
        //     using (var connection = CreateConnection())
        //     {
        //         connection.Open();

        //         var deleteSQL = string.Format(@"
        //             DELETE FROM items
        //             WHERE id = {0};", id);
        //         var res = await connection.ExecuteAsync(deleteSQL);
        //         return res;
        //     }
        // }
    }
}