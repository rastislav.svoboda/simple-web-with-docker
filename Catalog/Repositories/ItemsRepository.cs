using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalog.Entities;
using Dapper;
using Npgsql;

namespace Catalog.Repositories
{
    public class ItemsRepository
    {
        private NpgsqlConnection CreateConnection()
        {
            var connString = Environment.GetEnvironmentVariable("DB_CONNECTIONSTRING");

            //hack for debugging with only DB in docker
            if (connString == null)
                connString= "User ID=root;Password=password;Host=192.168.68.100;Port=5432;Database=docker;";

            return new NpgsqlConnection(connString);
        }

        public async Task<IReadOnlyCollection<Item>> GetAllAsync()
        {
            try
            {
                using (var connection = CreateConnection())
                {
                    var allItems = await connection.QueryAsync<Item>("SELECT * FROM items;");
                    return allItems.ToList();
                }
            }
            catch (Exception)
            {
                //_logger.LogCritical(ex, "dapper failed");
                return null;
            }
        }
       
        public async Task<Item> GetAsync(int id)
        {
            try
            {
                using (var connection = CreateConnection())
                {
                    var querySQL = string.Format(@"
                        SELECT * FROM items
                        WHERE id = {0};", id);
                    var item = await connection.QueryFirstOrDefaultAsync<Item>(querySQL);
                    return item;
                }
            }
            catch (Exception)
            {
                //_logger.LogCritical(ex, "dapper failed");
                return null;
            }
        }
        
        public async Task<int> CreateAsync(Item item)
        {
            using (var connection = CreateConnection())
            {
                connection.Open();

                var insertSQL = @"
                    INSERT INTO items (name, description, price)
                    VALUES (@name, @description, @price) returning id;";
                var res = await connection.QuerySingleAsync<int>(insertSQL, item);
                item.Id = res;
                return res;
            }
        }

        public async Task<int> UpdateAsync(Item item)
        {
            using (var connection = CreateConnection())
            {
                connection.Open();

                var updateSQL = @"
                    UPDATE items 
                    SET name = @name, description = @description, price = @price
                    WHERE id = @id;";
                var res = await connection.ExecuteAsync(updateSQL, item);
                return res;
            }
        }

        public async Task<int> RemoveAsync(int id)
        {
            using (var connection = CreateConnection())
            {
                connection.Open();

                var deleteSQL = string.Format(@"
                    DELETE FROM items
                    WHERE id = {0};", id);
                var res = await connection.ExecuteAsync(deleteSQL);
                return res;
            }
        }
    }
}