﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalog.Dtos;
using Catalog.Entities;
using Catalog.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ManifestsController : ControllerBase
    {
        private readonly ILogger<ManifestsController> _logger;
        private readonly ManifestsRepository manifestsRepository = new();

        public ManifestsController(ILogger<ManifestsController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<IEnumerable<ManifestDto>> GetAsync()
        {
            var manifests = await manifestsRepository.GetAllAsync();
            return manifests.Select(manifest => manifest.AsDto());
        }

        // // GET /items/{id}
        // [HttpGet("{id}")]
        // public async Task<ActionResult<ItemDto>> GetByIdAsync(int id)
        // {
        //     var item = await itemsRepository.GetAsync(id);
        //     if (item == null)
        //         return NotFound();

        //     return item.AsDto();
        // }

        // POST /items
        [HttpPost]
        public ActionResult<dynamic> Post(CreateManifestDto createManifestDto)
        {
            // var item = new 
            // {
            //     p_alias = createManifestDto.Alias,

            // };
            var res = manifestsRepository.Create(createManifestDto);
            
            return Ok();
            //return CreatedAtAction(nameof(GetByIdAsync), new { id = item.Id }, item);
        }

        // // PUT /items/{id}
        // [HttpPut("{id}")]
        // public async Task<IActionResult> PutAsync(int id, UpdateItemDto updateItemDto)
        // {
        //     var existingItem = await itemsRepository.GetAsync(id);
        //     if (existingItem == null)
        //         return NotFound();

        //     existingItem.Name = updateItemDto.Name;
        //     existingItem.Description = updateItemDto.Description;
        //     existingItem.Price = updateItemDto.Price;
        //     await itemsRepository.UpdateAsync(existingItem);

        //     return NoContent();
        // }

        // // DELETE /items/{id}
        // [HttpDelete("{id}")]
        // public async Task<IActionResult> DeleteAsync(int id)
        // {
        //     var item = await itemsRepository.GetAsync(id);
        //     if (item == null)
        //         return NotFound();
                
        //     await itemsRepository.RemoveAsync(item.Id);

        //     return NoContent();
        // }
    }
}
